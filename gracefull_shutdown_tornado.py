signal_received = False

def start_server(settings):
    application = create_app(settings)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)

    ioloop = tornado.ioloop.IOLoop.instance()

    def register_signal(sig, frame):
        global signal_received
        if hasattr(signal, 'Signals'):  # python3.5 has enums
            sig = signal.Signals(sig).name
        logger.info("%s received, stopping server" % sig)
        http_server.stop()  # no more requests are accepted
        signal_received = True

    def stop_on_signal():
        global signal_received
        if signal_received and not ioloop._callbacks:
            ioloop.stop()
            logger.info("IOLoop stopped")

    tornado.ioloop.PeriodicCallback(stop_on_signal, 1000).start()
    signal.signal(signal.SIGTERM, register_signal)
    ioloop.start()

    # access_log.propagate = False
    # # make sure access log is enabled even if error level is WARNING|ERROR
    # access_log.setLevel(logging.INFO)
    # stdout_handler = logging.StreamHandler(sys.stdout)
    # stdout_handler.setFormatter(AccessLogFormatter())
    # access_log.addHandler(stdout_handler)