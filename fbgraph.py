import facebook
import json

# https://developers.facebook.com/tools/explorer/

proxies = {
  'http': 'http://web-proxy.houston.hp.com:8080',
  'https': 'web-proxy.houston.hp.com:8080',
}

'''
/authentication/register?
email=wojtek.developer@gmail.com
&token=EAAGZCuZC5hgSkBAFPnSBQhrtLrc2GWeAAkIOm1ZADxk4bMZCayBFX8k52m6GLKgX16ZAQDz4WBuIzkZCZA7Jr98ZAsKZBbahXO5nojI7ZCqPdjD6Vdtmkp3f1zZAZA8mAZC9YKF2EZB5GDOZABfW5yZBhckmvGG5EVFUuXizhzw4gwnXcm2ndlfeSZCvoIo3TpQl29C7lBasZD
&usos_id=DEMO
&type=FB
'''

# access_token = 'EAACEdEose0cBAHSEQiOz0Wh1mwL5tGYaCWlZAXCiapOMYld3CkNaZAav54GjZBYLjZB90PEvrZCQPx4VfmbdjUT5ZBZB0YQ3FtCILVmzfr6ZCk10uEZBCzY0gBNnW402A0fRLUt0Pkn640M4QPsaoYVv6CqVBt5N5cYJ2czJC4LwckdZBNortAIAyy'

access_token = 'EAAGZCuZC5hgSkBAFPnSBQhrtLrc2GWeAAkIOm1ZADxk4bMZCayBFX8k52m6GLKgX16ZAQDz4WBuIzkZCZA7Jr98ZAsKZBbahXO5nojI7ZCqPdjD6Vdtmkp3f1zZAZA8mAZC9YKF2EZB5GDOZABfW5yZBhckmvGG5EVFUuXizhzw4gwnXcm2ndlfeSZCvoIo3TpQl29C7lBasZD'

graph = facebook.GraphAPI(access_token=access_token, proxies=proxies) # Initializing the object

profile = graph.get_object("me")     #  Extracting your own profile

print(profile)

from datetime import datetime, timedelta

now = datetime.now()

print(now)
print(timedelta(seconds=3600))
print(now + timedelta(seconds=3600))
