# coding=UTF-8

import logging
import urllib.parse as urllib_parse
import uuid

from tornado import gen, escape
from tornado import httpclient
from tornado.auth import OAuthMixin
from tornado.httpclient import HTTPRequest, HTTPError
from tornado.util import ObjectDict

PROXY_URL = None
PROXY_PORT = None


class CallerError(Exception):
    """Base error for this module."""
    pass


def get_client():
    if PROXY_URL and PROXY_PORT:
        httpclient.AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient',
                                             defaults=dict(proxy_host=PROXY_URL,
                                                           proxy_port=PROXY_PORT,
                                                           validate_cert=False,
                                                           max_clients=100))

    else:
        httpclient.AsyncHTTPClient.configure(None, max_clients=100)

    return httpclient.AsyncHTTPClient()


class OAuthCaller(OAuthMixin):
    _OAUTH_VERSION = '1.0a'
    _OAUTH_NO_CALLBACKS = False

    def _oauth_base_uri(self):
        return self._context.base_uri

    def _oauth_consumer_token(self):
        return self._context.consumer_token

    @staticmethod
    def get_auth_http_client():
        return get_client()

    @staticmethod
    def __build_context():
        context = ObjectDict()
        context.base_uri = 'https://usosapps.demo.usos.edu.pl/'
        context.consumer_token = dict(key='u9xbMGsj9rfER3QBPCBR', secret='f6TDvucnwDUJ8ZX7HCFa4kvNEFnE7MzByezgBy5v')
        context.access_token = dict(key='PCWKUJY2UYSdjHxN8eRy', secret='VKVgmVPPFkSHZKM5d2xC2TfEtvhZDwtEUMP6vfKS')

        return context

    @gen.coroutine
    def call(self, path, arguments=None, context=None):
        if not arguments:
            arguments = {}

        if not context:
            context = self.__build_context()

        self._context = context

        arguments['lang'] = 'pl'

        url = self._oauth_base_uri() + path

        # Add the OAuth resource request signature if we have credentials
        oauth = self._oauth_request_parameters(url, self._context.access_token, arguments)
        arguments.update(oauth)

        if arguments:
            url += "?" + urllib_parse.urlencode(arguments)

        client = self.get_auth_http_client()

        response = yield client.fetch(
            HTTPRequest(url=url, user_agent='OAuthCaller', connect_timeout=30, request_timeout=30))

        if response.code == 200 and 'application/json' in response.headers['Content-Type']:
            raise gen.Return(escape.json_decode(response.body))
        else:
            raise CallerError('Error code: {0} with body: {1} while fetching: {2}'.format(response.code,
                                                                                          response.body,
                                                                                          url))


@gen.coroutine
def main():
    caller = OAuthCaller()

    verify_token = str(uuid.uuid4())

    event_calls = list()
    for event_type in ['crstests/user_grade', 'grades/grade', 'crstests/user_point']:
        event_calls.append(caller.call(path='services/events/subscribe_event',
                                       arguments={
                                           'event_type': event_type,
                                           'callback_url': 'https://event.kujon.mobi/DEMO',
                                           'verify_token': verify_token
                                       }))
    event_result = yield event_calls

    logging.info('subscribe for resulted in {0}'.format(event_result))

    try:
        unsubscribe = yield caller.call(path='services/events/unsubscribe')
        logging.info('unsubscribe resulted in {0}'.format(unsubscribe))
    except HTTPError as err:
        logging.error('code: {0} reason: {1} body: {2}'.format(err.code, err.response.reason, err.response.body))
    except Exception as ex:
        logging.exception(ex)


if __name__ == '__main__':
    from tornado import ioloop
    from tornado.options import parse_command_line

    parse_command_line()
    logging.getLogger().setLevel(logging.DEBUG)
    io_loop = ioloop.IOLoop.current()
    io_loop.run_sync(main)
