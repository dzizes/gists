import logging
import time
import random

from tornado import gen, queues
from tornado.concurrent import run_on_executor, futures
from tornado.ioloop import IOLoop
from tornado.options import parse_command_line

MAX_WORKERS = 10
SLEEP = 1


class TaskRunner(object):
    def __init__(self):
        self.executor = futures.ThreadPoolExecutor(MAX_WORKERS)
        self.queue = queues.Queue(maxsize=MAX_WORKERS)
        self.counter = -1
        self.processing = []

    # @gen.coroutine
    @run_on_executor
    def long_running_task(self, job):
        try:
            self.processing.append(job)
            s = random.randint(10, 15)
            logging.info('long_running_task going to sleep job number {0} for {1} seconds '.format(job, s))
            # yield gen.sleep(s)
            time.sleep(s)
            logging.info('long_running_task waking up job number {0}'.format(job))
        finally:
            self.processing.remove(job)

    @gen.coroutine
    def load_work(self):
        # read mongo collection and for each document create queue entry
        for i in range(5):
            if len(self.processing) >= MAX_WORKERS:
                break

            self.counter += 1
            yield self.queue.put(self.counter)
            logging.info('producing {0} queue size: {1} processing: {2}'.format(self.counter, self.queue.qsize(), len(self.processing)))

        raise gen.Return(True)

    @gen.coroutine
    def worker(self):
        while True:
            try:
                job = yield self.queue.get()
                self.long_running_task(job)
            except Exception, ex:
                logging.exception(ex)
            finally:
                self.queue.task_done()

    @gen.coroutine
    def producer(self):
        while True:
            yield self.load_work()
            yield gen.sleep(SLEEP)

    @gen.coroutine
    def workers(self):
        IOLoop.current().spawn_callback(self.producer)
        futures = [self.worker() for _ in range(MAX_WORKERS)]
        yield futures


if __name__ == '__main__':
    parse_command_line()

    logging.getLogger().setLevel(logging.DEBUG)

    loop = IOLoop()
    taskRunner = TaskRunner()
    loop.run_sync(taskRunner.workers)
