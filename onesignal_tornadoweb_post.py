# coding=UTF-8

import json
import logging

from tornado import gen
from tornado import httpclient
from tornado.httputil import HTTPHeaders

try:
    import urllib.parse as urllib_parse  # py3
except ImportError:
    import urllib as urllib_parse  # py2

ENCODING = 'UTF-8'
PROXY_URL = 'proxy.bbn.hp.com'
PROXY_PORT = 8080
APP_ID = 'f01a20f9-bbe7-4c89-a017-bf8930c61cf4'
URL = 'https://onesignal.com/api/v1/notifications'
SIGNAL_AUTHORIZATION = 'Basic NTdhODU0YjAtOTZhNy00YWRlLTgyYTktZWI3NDlkYTM2ZTEx'


def http_client():
    if PROXY_URL and PROXY_PORT:
        httpclient.AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient',
                                             defaults=dict(proxy_host=PROXY_URL,
                                                           proxy_port=PROXY_PORT,
                                                           validate_cert=False,
                                                           max_clients=100))

    else:
        httpclient.AsyncHTTPClient.configure(None, max_clients=100)

    return httpclient.AsyncHTTPClient()


@gen.coroutine
def signal_get(url):
    client = http_client()
    response_body = None
    try:
        headers = HTTPHeaders({
            'Content-Type': 'application/json',
            'Authorization': SIGNAL_AUTHORIZATION
        })

        response = yield client.fetch(httpclient.HTTPRequest(url=url,
                                                             method='GET',
                                                             headers=headers,
                                                             connect_timeout=300,
                                                             request_timeout=300))

        logging.info('response code: {0} reason: {1}'.format(response.code, response.reason))
        response_body = json.loads(response.body.decode(ENCODING))

    except httpclient.HTTPError as ex:
        logging.exception(ex)
    except Exception as ex:
        logging.exception(ex)
    finally:
        client.close()
        raise gen.Return(response_body)


@gen.coroutine
def send_message(message, email_reciepient, language='en'):
    client = http_client()
    response_body = None
    try:
        headers = HTTPHeaders({
            'Content-Type': 'application/json',
            'Authorization': SIGNAL_AUTHORIZATION
        })

        body = json.dumps({
            'app_id': APP_ID,
            # 'included_segments': ['All'],
            # 'include_player_ids': ['a7286e6d-5eb8-4c71-9d05-a7a18b197b21', ],
            'tags': [{'user_email': email_reciepient}],
            # 'id': '5842429a-248d-4e73-8727-bdb8468ddaac',
            # 'tags': [{"key": "user_email", "relation": "=", email_reciepient: "true"}],
            'contents': {'en': 'message in en'}
        })

        response = yield client.fetch(httpclient.HTTPRequest(url=URL,
                                                             method='POST',
                                                             headers=headers,
                                                             body=body,
                                                             connect_timeout=300,
                                                             request_timeout=300))

        logging.info('response code: {0} reason: {1}'.format(response.code, response.reason))
        response_body = json.loads(response.body.decode(ENCODING))

    except httpclient.HTTPError as ex:
        logging.exception(ex)
    except Exception as ex:
        logging.exception(ex)
    finally:
        client.close()
        raise gen.Return(response_body)


@gen.coroutine
def main():
    response = yield send_message('Wiadomość testowa pojedyńczego użytkownika.', 'dzizes451@gmail.com', language='pl')
    logging.info(response)

    '''
    response = yield signal_get(URL + "?" + urllib_parse.urlencode({'app_id': APP_ID}))
    logging.info(response)


    response = yield signal_get("https://onesignal.com/api/v1/players?" + urllib_parse.urlencode({'app_id': APP_ID, 'limit': 300, 'offset': 0}))
    logging.info(response)
    '''

if __name__ == '__main__':
    from tornado import ioloop
    from tornado.options import parse_command_line

    parse_command_line()
    logging.getLogger().setLevel(logging.DEBUG)
    io_loop = ioloop.IOLoop.current()
    io_loop.run_sync(main)
