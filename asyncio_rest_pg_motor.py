import ujson

import aiopg
import motor.motor_asyncio
from aiohttp import web
from aiopg.sa import create_engine

import settings


async def db_handler(app, handler):
    async def middleware(request):
        client = motor.motor_asyncio.AsyncIOMotorClient(settings.MONGO_HOST)
        request.db = client[settings.MONGO_DB_NAME]

        request.pg = await create_engine(user=settings.DATABASE_USERNAME,
                                         database=settings.DATABASE_NAME,
                                         host=settings.DATABASE_HOST,
                                         password=settings.DATABASE_PASSWORD)
        request.pool = await aiopg.create_pool(settings.DSN)
        response = await handler(request)
        return response

    return middleware


class BaseView(web.View):
    def _json(self, data):
        return web.json_response(ujson.dumps(data, sort_keys=True, indent=4))

    async def query(self, query):
        async with self.request.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query)
                result = []
                async for row in cur:
                    result.append(row)
                return result

    async def find(self, collection):
        messages = self.request.db[collection].find({}, {"_id": 0})
        response = await messages.to_list(length=None)
        return response


class MongoView(BaseView):
    async def get(self):
        collection = self.request.match_info['name']

        response = await self.find(collection)

        return self._json(response)


class PgView(BaseView):
    async def get(self):
        name = self.request.match_info['name']

        count = await self.query('SELECT COUNT(*) FROM {0}'.format(name))

        response = {
            'name': name,
            'count': count
        }
        return self._json(response)


if __name__ == '__main__':
    app = web.Application(middlewares=[db_handler, ])
    app.router.add_route('GET', '/mg/{name}', MongoView)
    app.router.add_route('GET', '/pg/{name}', PgView)

    # app.client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_HOST)
    # app.db = app.client[MONGO_DB_NAME]

    web.run_app(app)
