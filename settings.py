
DATABASE_USERNAME = 'postgres'
DATABASE_NAME = 'swde'
DATABASE_HOST = 'localpginstance'
DATABASE_PASSWORD = 'postgres'

DSN = 'dbname={0} user={1} password={2} host={3}'.format(
    DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_HOST)

MONGO_HOST = 'mongodb://localmongoinstance/kujon-dev'
MONGO_DB_NAME = 'kujon-dev'