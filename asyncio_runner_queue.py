import asyncio
import logging

import motor.motor_asyncio

MONGODB_URI = 'mongodb://localmongoinstance/kujon-dev'
MONGODB_NAME = 'kujon-dev'


class QueueRunner(object):
    def __init__(self, loop=None):
        if not loop:
            loop = asyncio.get_event_loop()

        self.loop = loop
        self.COUNTER = 0
        self.SLEEP = 2
        self.running = True
        self.queue = asyncio.Queue(maxsize=100, loop=self.loop)
        self.db = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URI)[MONGODB_NAME]

    async def produce(self):
        while self.running:
            self.COUNTER += 1
            cursor = self.db['collection'].find()

            async for job in cursor:
                await self.queue.put(job)
                logging.info('producing job {0}'.format(self.COUNTER))

            await asyncio.sleep(self.SLEEP)

    async def consume(self):
        while True:
            job = await self.queue.get()
            logging.info('consuming job {0}'.format(job))

            try:
                '''DO STH'''
            except Exception as ex:
                logging.exception(ex)


if __name__ == '__main__':

    try:
        loop = asyncio.get_event_loop()
        runner = QueueRunner(loop)

        asyncio.ensure_future(runner.produce(), loop=loop)
        loop.run_until_complete(runner.consume())
    finally:
        loop.close()
