import asyncio
import aiopg

dsn = 'dbname=swde user=postgres password=postgres host=localpginstance'

async def go():
    pool = await aiopg.create_pool(dsn)
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            # await cur.execute("SELECT 1")
            await cur.execute("select * from osm.buildings")
            ret = []
            async for row in cur:
                ret.append(row)
                print(row)
            assert ret == [(1,)]

loop = asyncio.get_event_loop()
loop.run_until_complete(go())