import logging
from datetime import datetime
from tornado import gen
from tornado.ioloop import IOLoop
from random import randint
from tornado.options import parse_command_line

SLEEP = 2


@gen.coroutine
def do_task(boo, foo):
    yield gen.sleep(SLEEP)
    logging.info('done doing boo: {0} and foo: {1}'.format(boo, foo))
    raise gen.Return(None)


@gen.coroutine
def run():
    a = []
    for i in range(0, 11):
        a.append(do_task(i, randint(20, 31)))
    yield a


if __name__ == '__main__':
    parse_command_line()

    start = datetime.now()

    io_loop = IOLoop.current()
    io_loop.run_sync(run)

    seconds = datetime.now() - start
    logging.info('done after {0} seconds'.format(seconds))
