# coding=UTF-8

import asyncio
import logging

import aiohttp
import motor.motor_asyncio
from tornado.options import parse_command_line

SLEEP = 2
COLLECTION_NAME = 'usosinstances'
MONGODB_URI = 'mongodb://localmongoinstance/kujon-dev'
MONGODB_NAME = 'kujon-dev'
PROXY_PORT = None
PROXY_URL = None
LIMIT = 100


class Error(Exception):
    """Base error for this module."""
    pass


class ForeverChecker(object):
    def __init__(self):
        self.db = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URI)[MONGODB_NAME]

        if PROXY_PORT and PROXY_URL:
            self.conector = aiohttp.ProxyConnector('{0}:{1}'.format(PROXY_URL, PROXY_PORT),
                                                   verify_ssl=False, limit=LIMIT)
        else:
            self.conector = aiohttp.TCPConnector(verify_ssl=False, limit=LIMIT)

    async def forever(self):
        session = aiohttp.ClientSession(connector=self.conector)

        while True:
            cursor = self.db[COLLECTION_NAME].find({'enabled': True})
            async for doc in cursor:
                try:
                    url = doc['url'] + 'services/courses/classtypes_index'

                    async with session.get(url) as response:
                        if response.status != 200 and 'application/json' not in response.headers['Content-Type']:
                            raise Error('buuu!')
                        else:
                            json = await response.json()
                            logging.debug(json)  # do some mongo insert

                except Exception as ex:
                    logging.exception(ex)

            await asyncio.sleep(SLEEP)


def main():
    parse_command_line()
    logging.getLogger().setLevel('DEBUG')

    loop = asyncio.get_event_loop()
    checker = ForeverChecker()
    loop.run_until_complete(checker.forever())


if __name__ == '__main__':
    main()
