# coding=UTF-8

import logging

import momoko
from tornado import web, gen
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import parse_command_line

DATABASE_NAME = 'swde'
DATABASE_USER = 'postgres'
DATABASE_HOST = 'localpginstance'
DATABASE_PORT = 5432
DATABASE_PASSWORD = 'postgres'
DATABASE_SCHEMA_NAME = 'swde_import'

DSN = 'dbname={0} user={1} password={2} host={3} port={4}'.format(
    DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD, DATABASE_HOST, DATABASE_PORT
)


class BaseHandler(web.RequestHandler):
    @property
    def db(self):
        return self.application.db


class TutorialHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        cursor = yield self.db.execute("select * from swde_import.implogtab;")
        # logging.debug(cursor.fetchall())
        self.write("Results: {0}".format(cursor.fetchall()))
        self.finish()


if __name__ == '__main__':
    parse_command_line()
    logging.getLogger().setLevel(logging.DEBUG)

    application = web.Application([
        (r'/', TutorialHandler)
    ], debug=True)

    ioloop = IOLoop.instance()

    application.db = momoko.Pool(
        dsn=DSN,
        size=2,
        ioloop=ioloop,
    )

    # this is a one way to run ioloop in sync
    future = application.db.connect()
    ioloop.add_future(future, lambda f: ioloop.stop())
    ioloop.start()
    future.result()  # raises exception on connection error

    http_server = HTTPServer(application)
    http_server.listen(8889, 'localhost')
    ioloop.start()
