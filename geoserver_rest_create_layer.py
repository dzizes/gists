# coding=UTF-8

import json
from urllib.parse import urljoin

import requests

REST_URL = 'http://192.168.56.101:8080/geoserver/rest/'
CREADENTIALS = ('admin', 'geoserver')
HEADERS_GET = {'content-type': 'application/json'}
HEADERS_POST = {'Accept': 'application/json'}


def create_workspace(ws_name):
    print('creating workspace: {0}'.format(ws_name))
    resource = 'workspaces'
    payload = {'workspace': {'name': ws_name}}
    request_url = urljoin(REST_URL, resource)

    r = requests.post(
        request_url,
        data=json.dumps(payload),
        headers=HEADERS_GET,
        auth=CREADENTIALS
    )

    r.raise_for_status()

    print('workspace {0} created'.format(ws_name))


def exists_workspace(ws_name):
    resource = 'workspaces/' + ws_name

    request_url = urljoin(REST_URL, resource)

    try:
        r = requests.get(
            request_url,
            headers=HEADERS_POST,
            auth=CREADENTIALS
        )

        print('workspace: {0}'.format(r.text))

        return True
    except requests.exceptions.HTTPError as ex:
        print(ex)
        return False


def main():
    workspace_name = 'rest_workspace'

    if not exists_workspace(workspace_name):
        try:
            create_workspace(workspace_name)
        except requests.exceptions.HTTPError as ex:
            print(ex)



if __name__ == '__main__':
    main()

