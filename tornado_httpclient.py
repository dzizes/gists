# coding=UTF-8

import asyncio
import logging

import motor
from tornado import httpclient, escape
from tornado.httpclient import HTTPRequest
from tornado.ioloop import IOLoop
from tornado.options import parse_command_line

SLEEP = 2
COLLECTION_NAME = 'usosinstances'
MONGODB_URI = 'mongodb://localmongoinstance/kujon-dev'
MONGODB_NAME = 'kujon-dev'
PROXY_PORT = None
PROXY_URL = None
LIMIT = 100
CLIENT_TYPE = "tornado.curl_httpclient.CurlAsyncHTTPClient"


class Error(Exception):
    """Base error for this module."""
    pass


def http_client():
    httpclient.AsyncHTTPClient.configure(None,
                                         defaults=dict(proxy_host=PROXY_PORT,
                                                       proxy_port=PROXY_URL,
                                                       validate_cert=False),
                                         max_clients=LIMIT)

    return httpclient.AsyncHTTPClient()


class ForeverChecker(object):
    def __init__(self):
        self.db = motor.motor_tornado.MotorClient(MONGODB_URI)[MONGODB_NAME]
        self.client = http_client()

    async def forever(self):

        while True:
            cursor = self.db[COLLECTION_NAME].find({'enabled': True})
            async for doc in cursor:
                try:
                    url = doc['url'] + 'services/courses/classtypes_index'

                    response = await self.client.fetch(HTTPRequest(url=url))

                    if response.code != 200 and 'application/json' not in response.headers['Content-Type']:
                        raise Error('buuu!')
                    else:
                        json = escape.json_decode(response.body)
                        logging.debug(json)  # do some mongo insert

                except Exception as ex:
                    logging.exception(ex)

            await asyncio.sleep(SLEEP)


def main():
    parse_command_line()

    logging.getLogger().setLevel('DEBUG')

    checker = ForeverChecker()

    io_loop = IOLoop.current()
    io_loop.run_sync(checker.forever)


if __name__ == '__main__':
    main()
