# coding=UTF-8

import logging

from tornado import gen, escape
from tornado import httpclient

PROXY_URL = 'web-proxy.houston.hp.com'
PROXY_PORT = 8080


class Error(Exception):
    """Base error for this module."""
    pass


def get_client():
    if PROXY_URL and PROXY_PORT:
        httpclient.AsyncHTTPClient.configure('tornado.curl_httpclient.CurlAsyncHTTPClient',
                                             defaults=dict(proxy_host=PROXY_URL,
                                                           proxy_port=PROXY_PORT,
                                                           validate_cert=False,
                                                           max_clients=100))

    else:
        httpclient.AsyncHTTPClient.configure(None, max_clients=100)

    return httpclient.AsyncHTTPClient()


@gen.coroutine
def call(given_url):
    response = yield get_client().fetch(given_url)
    if response.code == 200 and 'application/json' in response.headers['Content-Type']:
        raise gen.Return(escape.json_decode(response.body))
    else:
        raise Error('Error code: {0} with body: {1} while fetching: {2}'.format(response.code,
                                                                                response.body,
                                                                                given_url))

@gen.coroutine
def main():
    try:
        response = yield call('https://usosapps.demo.usos.edu.pl/services/courses/classtypes_index')
        logging.info(response)
    except Exception as ex:
        logging.exception(ex)   # ?

    '''
        multiple call yielding
    '''


if __name__ == '__main__':
    from tornado import ioloop
    from tornado.options import parse_command_line

    parse_command_line()
    logging.getLogger().setLevel(logging.DEBUG)
    io_loop = ioloop.IOLoop.current()
    io_loop.run_sync(main)
