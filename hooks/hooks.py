import logging
import json
import ssl

import tornado.ioloop
import tornado.web
import tornado.gen
from tornado.httpserver import HTTPServer

from tornado.options import parse_command_line, define, options
from commons import settings

define("port", default=settings.HOOKS_PORT, help="run on the given port", type=int)


class MainHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ("POST", )

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def prepare(self):
        logging.info('processing hook start')

        x_real_ip = self.request.headers.get("X-Real-IP")
        remote_ip = x_real_ip or self.request.remote_ip
        logging.debug("remote_ip: %r", remote_ip)

        signature = self.request.headers.get('X-Hub-Signature')
        event = self.request.headers.get('X-GitHub-Event')
        delivery = self.request.headers.get('X-GitHub-Delivery')

        logging.debug("signature: %r event: %r delivery: %r", signature, event, delivery)

        # if not signature:
        #     self.write_error(401, "Bad post request")
        #     return

        # if not check_signature(signature, key, payload):
        #     self.write_error(401, "Wrong signature")

        # if not event:
        #     self.write_error(401, 'Missing header: X-GitHub-Event')
        #     return
        # elif not delivery:
        #     self.write_error(401, 'Missing header: X-GitHub-Delivery')
        #     return

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self, *args, **kwargs):
        data = json.loads(self.request.body)
        logging.info('post data: ' + data)

    def on_finish(self):
        logging.info('processing hook finish')


class Application(tornado.web.Application):

    def __init__(self):
        __handlers = [
            (r"/", MainHandler),
        ]

        __settings = dict(
            debug=settings.DEBUG,
            autoreload=settings.RELOAD,
            xheaders=True,
        )

        tornado.web.Application.__init__(self, __handlers, **__settings)


if __name__ == "__main__":
    parse_command_line()

    if settings.DEBUG:
        logging.getLogger().setLevel(logging.DEBUG)

    application = Application()

    if settings.SSL_CERT and settings.SSL_KEY:
        ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_ctx.load_cert_chain(settings.SSL_CERT, settings.SSL_KEY)

        server = HTTPServer(application, ssl_options=ssl_ctx)
        server.listen(options.port)
        logging.info('SSL ENABLED FOR HOOKS')
    else:
        application.listen(options.port)
        logging.info('SSL DISABLED FOR HOOKS')

    logging.info(settings.HOOKS_PORT)
    tornado.ioloop.IOLoop.current().start()
